#!/bin/bash -xe

mosquitto_pub -h homeassistant -t firmware/foam-soap-dispenser -d -r -f .pio/build/esp32dev/firmware.bin
#mosquitto_pub -h homeassistant -t ohm/seifenspender/config -r -f config.json

mosquitto_pub -h homeassistant -t ohm/seifenspender/trigger -d -r -m firmwareupdate
#mosquitto_pub -h homeassistant -t ohm/seifenspender/trigger -r -n


