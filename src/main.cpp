#include <stdio.h>
#include <Arduino.h>
#include <WiFi.h>
#include "soc/soc.h"
#include "soc/rtc_cntl_reg.h"
#include "unistd.h"

#include <Arduino.h>
//#include <EEPROM.h>
#include <Preferences.h>
//#include "crc.h"
#include "config.h"

// to fix error: no type named 'type' in 'struct ArduinoJson::Internals::EnableIf<false, String>'
//#define ARDUINOJSON_ENABLE_ARDUINO_STRING 1
#include <ArduinoJson.h>

#include <WiFi.h>
#include <string>

//#include "ESPAsyncTCP.h"
#include <AsyncTCP.h>
#include <ESPAsyncWebServer.h>
#include <AsyncElegantOTA.h>
#include <ESPAsyncWiFiManager.h>
#include <AsyncMqttClient.h>
#include <ESPmDNS.h>

#define ENABLE_PRINT

#ifndef ENABLE_PRINT
// disable Serial output
#define Serial SerialDummy
static class
{
public:
    void begin(...) {}
    void print(...) {}
    void println(...) {}
    void setDebugOutput(...) {}
    void flush(...) {}
    void printf(...) {}
} Serial;

#endif

#define DEBUGOUT // Comment to enable debug output

#define DBG_OUTPUT Serial

#ifdef DEBUGOUT
#define DEBUGLOG(...) DBG_OUTPUT.printf(__VA_ARGS__)
#else
#define DEBUGLOG(...)
#endif

#define XSTR(x) STR(x)
#define STR(x) #x

#ifndef BUILDTIME
#define BUILDTIME "none"
#endif
#pragma message("BUILDTIME=" XSTR(BUILDTIME))

extern unsigned long crc(uint8_t *blob, unsigned int size);

Preferences preferences;

AsyncMqttClient mqttClient;
AsyncWebServer server(80);
DNSServer dns;

//hostConfig myhostConfig = {};

appConfig myappConfig = {};
extern void updateFirmware(AsyncWebServerRequest *request, String filename, size_t index, uint8_t *data, size_t len, bool final);

//flag for saving data
bool shouldSaveConfig = false;
//callback notifying us of the need to save config
void saveConfigCallback()
{
    Serial.println(F("Save config\n"));
    shouldSaveConfig = true;
}

const char *version = "0.8";
bool doing_update = false;
bool doing_restart = false;
bool force_config = false;
bool send_event_message = false;
bool send_status_message = true;
unsigned long last_status_message_millis = 0;
bool mdns_startet = false;
int nrOfServices = 0;
IPAddress localIP;
long rssi;
int do_subscripe_firmwaretopic_id = 0;
bool has_valid_appconfig = false;
int last_mqtt_package_id = 0;
int last_publish_success_id = 0;
bool mqtt_do_connection = false;
const char *buildtime = (const char *)BUILDTIME;
//uint8_t timeout_seconds = TIMEOUT_SECONDS_DEFAULT;
//unsigned long timeout_millis = 0;
unsigned long last_mqtt_connect_try = 0;

extern bool pump_enable;

void *jsonMessage;
StaticJsonDocument<200> doc_event;
String topicPrefix;

void connectToMqtt()
{
    mqtt_do_connection = false;
    last_mqtt_connect_try = millis();
    if (!mqttClient.connected())
    {
        Serial.println("Connecting to MQTT...");
        mqttClient.setMaxTopicLength(MAXTOPICLENGTH);
        topicPrefix = DEFAULT_HOSTNAME + String("/") + localIP.toString();
        mqttClient.connect();
    }
    /*
    else
    {
        //Serial.println("MQTT already connected!");
        mqtt_do_connection = false;
    }
    */
}

bool startMdns(){
    if(!MDNS.begin(DEFAULT_HOSTNAME)) {
        Serial.println("Error starting mDNS");
        return false;
    }
    mdns_startet = true;
    return true;
}
void WiFiEvent(WiFiEvent_t event)
{
    Serial.printf("[WiFi-event] event: %d\n", event);
    switch (event)
    {
    case SYSTEM_EVENT_STA_GOT_IP:
        Serial.println("WiFi connected");
        Serial.println("IP address: ");
        Serial.println(WiFi.localIP());
        localIP = WiFi.localIP();
        rssi = WiFi.RSSI();
        Serial.print("RSSI:");
        Serial.println(rssi);
        startMdns();
        //connectToMqtt();
        break;
        /*case SYSTEM_EVENT_STA_DISCONNECTED:
        Serial.println("WiFi lost connection");
        xTimerStop(mqttReconnectTimer, 0); // ensure we don't reconnect to MQTT while reconnecting to Wi-Fi
		    xTimerStart(wifiReconnectTimer, 0);
        break;*/

    default:
        break;
    }
}

uint16_t publishStatus()
{
    //String topicPrefix = String(myhostConfig.configTopic);
    //String topicPrefix = String(DEFAULT_HOSTNAME);

    StaticJsonDocument<500> doc;
    //doc["vbat_begin"] = startvbat;
    //doc["vbat_late"] = vbat;
    doc["has_valid_appconfig"] = has_valid_appconfig;
    doc["rssi"] = rssi;
    doc["version"] = version;
    doc["buildtime"] = buildtime;
    doc["ir_pre_maesure"] = (uint16_t)ir_pre_maesure;
    doc["ir_maesure"] = (uint16_t)ir_maesure;
    doc["ir_diff"] = (uint16_t)ir_diff;
    doc["button"] = digitalRead(BUTTON);

    String payload;
    serializeJson(doc, payload);
    uint16_t packetIdPub1 = mqttClient.publish(
        String(topicPrefix + "/status").c_str(), 0, true,
        payload.c_str());
    Serial.print("Publishing at QoS 1, packetId: ");
    Serial.println(packetIdPub1);
    return packetIdPub1;
}

uint16_t publishEvent(StaticJsonDocument<200> doc)
{
    send_event_message = false;
    //String topicPrefix = String(myhostConfig.configTopic);
    //String topicPrefix = String(DEFAULT_HOSTNAME);

    /*
    StaticJsonDocument<200> doc;
    doc["motor"] = true;
    doc["ir_pre_maesure"] = ir_pre_maesure;
    doc["ir_maesure"] = ir_maesure;
    doc["ir_diff"] = ir_diff;
    */
    doc["millis"] = millis();

    String payload;
    serializeJson(doc, payload);
    uint16_t packetIdPub1 = mqttClient.publish(
        String(topicPrefix + "/event").c_str(), 0, false,
        payload.c_str());
    Serial.print("Publishing at QoS 1, packetId: ");
    Serial.println(packetIdPub1);
    return packetIdPub1;
}

void onMqttDisconnect(AsyncMqttClientDisconnectReason reason)
{
    Serial.print("Disconnect MQTT, reason: ");
    Serial.println((int)reason);
    //mqtt_do_connection = false;
    digitalWrite(LED_BLUE, HIGH);
    digitalWrite(LED_RED, HIGH);
    //doing_update = false;
    /*if (Update.isRunning() > 0)
    {
        Update.abort();
    }
    */
}

void onMqttConnect(bool sessionPresent)
{
    Serial.println("Connected to MQTT.");
    Serial.print("Session present: ");
    Serial.println(sessionPresent);

    //String topicPrefix = String(myhostConfig.configTopic);
    //String topicPrefix = String(DEFAULT_HOSTNAME);

    uint16_t packetIdSub2 = mqttClient.subscribe(
        String(topicPrefix + "/config").c_str(), 2);
    Serial.print("Subscribing at QoS 2, packetId: ");
    Serial.println(packetIdSub2);

    uint16_t packetIdSub3 = mqttClient.subscribe(
        String(topicPrefix + "/trigger").c_str(), 2);
    Serial.print("Subscribing at QoS 2, packetId: ");
    Serial.println(packetIdSub3);
    send_status_message = true;

}


void onMqttSubscribe(uint16_t packetId, uint8_t qos)
{
    Serial.println("Subscribe acknowledged.");
    Serial.print("  packetId: ");
    Serial.println(packetId);
    Serial.print("  qos: ");
    Serial.println(qos);
}

void onMqttUnsubscribe(uint16_t packetId)
{
    Serial.println("Unsubscribe acknowledged.");
    Serial.print("  packetId: ");
    Serial.println(packetId);
}

void saveAppConfig()
{
    unsigned long newcrc = crc((uint8_t *)&myappConfig, sizeof(appConfig));
    if (newcrc != myappConfig.crc)
    {
        myappConfig.crc = newcrc;
        /*
        EEPROM.put(sizeof(hostConfig), myappConfig);
        EEPROM.commit();
        */
        preferences.begin("appconfig");
        preferences.putBytes("appconfig", &myappConfig, sizeof(appConfig));
        preferences.end();
        Serial.println(myappConfig.crc);
    }
}

void onMqttMessage(char *topic, char *payload, AsyncMqttClientMessageProperties properties, size_t len, size_t index, size_t total)
{
    if (!doing_update)
    {

        Serial.print("Publish received, ");
        Serial.print("  topic: ");
        Serial.print(topic);
        Serial.print(",  qos: ");
        Serial.print(properties.qos);
        Serial.print(",  dup: ");
        Serial.print(properties.dup);
        Serial.print(",  retain: ");
        Serial.print(properties.retain);
    }
    Serial.print(",  len: ");
    Serial.print(len);
    Serial.print(",  index: ");
    Serial.print(index);
    Serial.print(",  total: ");
    Serial.println(total);

    //String topicPrefix = String(myhostConfig.configTopic);
    //String topicPrefix = String(DEFAULT_HOSTNAME);

    if (String(topic) == String(topicPrefix + "/config") && total > 0)
    {
        if (index == 0)
        {
            jsonMessage = malloc(total + 1);
            memset(jsonMessage + total, 0, 1);
        }
        memcpy(jsonMessage + index, payload, len);
        if (index + len == total)
        {
            DynamicJsonDocument root(total + 128);
            auto jsonerror = deserializeJson(root, jsonMessage);
            if (!jsonerror)
            {
                Serial.println("JSON parse success");
                JsonObject rootobj = root.as<JsonObject>();
                if (rootobj.containsKey("ir_off_difference"))
                {
                    uint16_t value = rootobj["ir_off_difference"];
                    Serial.print("set new ir_off_difference config: ");
                    Serial.println(value);
                    myappConfig.ir_off_difference = value;
                }
                if (rootobj.containsKey("ir_off_pre_measure_min"))
                {
                    uint16_t value = rootobj["ir_off_pre_measure_min"];
                    Serial.print("set new ir_off_pre_measure_min config: ");
                    Serial.println(value);
                    myappConfig.ir_off_pre_measure_min = value;
                }
                if (rootobj.containsKey("ir_off_measure_max"))
                {
                    uint16_t value = rootobj["ir_off_measure_max"];
                    Serial.print("set new ir_off_measure_max config: ");
                    Serial.println(value);
                    myappConfig.ir_off_measure_max = value;
                }
                if (rootobj.containsKey("ir_off_difference"))
                {
                    uint16_t value = rootobj["ir_off_difference"];
                    Serial.print("set new ir_off_difference config: ");
                    Serial.println(value);
                    myappConfig.ir_off_difference = value;
                }
                if (rootobj.containsKey("ir_off_pre_measure_min"))
                {
                    uint16_t value = rootobj["ir_off_pre_measure_min"];
                    Serial.print("set new ir_off_pre_measure_min config: ");
                    Serial.println(value);
                    myappConfig.ir_off_pre_measure_min = value;
                }
                if (rootobj.containsKey("ir_off_measure_max"))
                {
                    uint16_t value = rootobj["ir_off_measure_max"];
                    Serial.print("set new ir_off_measure_max config: ");
                    Serial.println(value);
                    myappConfig.ir_off_measure_max = value;
                }
                if (rootobj.containsKey("core_led_delay_ms"))
                {
                    uint16_t value = rootobj["core_led_delay_ms"];
                    Serial.print("set new core_led_delay_ms config: ");
                    Serial.println(value);
                    myappConfig.core_led_delay_ms = value;
                }
                if (rootobj.containsKey("power_on_duration_ms"))
                {
                    uint16_t value = rootobj["power_on_duration_ms"];
                    Serial.print("set new power_on_duration_ms config: ");
                    Serial.println(value);
                    myappConfig.power_on_duration_ms = value;
                }
                if (rootobj.containsKey("firmware_topic"))
                {
                    String firmwaretopic = rootobj["firmware_topic"];
                    if (firmwaretopic.length() < MAXTOPICLENGTH)
                    {
                        Serial.print("set new firmware topic from config: ");
                        Serial.println(firmwaretopic);
                        strcpy(myappConfig.firmwaretopic, firmwaretopic.c_str());
                    }
                }
                saveAppConfig();
                send_status_message = true;
            }
            else
            {
                Serial.print("JSON parse failed: ");
                Serial.println(jsonerror.c_str());
            }
        }
        free(jsonMessage);
    }

    if (String(topic) == String(topicPrefix + "/trigger") && total > 0)
    {
        //doing_update = true;
        char s_payload[len + 1];
        s_payload[len] = '\0';
        strncpy(s_payload, payload, len);

        if (properties.retain){
            uint16_t packetIdPub1 = mqttClient.publish(
                String(topicPrefix + "/trigger").c_str(), 0, true);
            Serial.print("Publishing at QoS 1, packetId: ");
            Serial.println(packetIdPub1);
            if (String(s_payload) == String("firmwareupdate") &&
                String(myappConfig.firmwaretopic) != "")
            {
                digitalWrite(LED_GREEN, LOW);
                doing_update = true;
                do_subscripe_firmwaretopic_id = packetIdPub1;
            }
        }

        if (String(s_payload) == String("forceconfig"))
        {
            force_config = true;
        }
        if (String(s_payload) == String("restart"))
        {
            doing_restart = true;
        }
        if (String(s_payload) == String("enable"))
        {
            pump_enable = true;
        }
        if (String(s_payload) == String("disable"))
        {
            pump_enable = false;
        }

    }

    if (String(myappConfig.firmwaretopic) != "" &&
        String(topic) == String(myappConfig.firmwaretopic) &&
        total > 0)
    {
        digitalWrite(LED_GREEN, LOW);
        doing_update = true;
        int _lastError = 0;
        // start update
        if (index == 0)
        {
            //Update.runAsync(true);
            if (!Update.begin(total, U_FLASH))
            {
                _lastError = Update.getError();
#ifdef ENABLE_PRINT
                Update.printError(Serial);
#endif
                // ignore the rest
                topic[0] = 0;
            }
        }
        // do update in chunks
        if (!_lastError)
        {
            if (Update.write((uint8_t *)payload, len) != len)
            {
                _lastError = Update.getError();
#ifdef ENABLE_PRINT
                Update.printError(Serial);
#endif
                // ignore the rest
                topic[0] = 0;
            }
        }
        // finish update
        if (!_lastError)
        {
            if (index + len == total)
            {
                if (!Update.end())
                {
                    _lastError = Update.getError();
                    digitalWrite(LED_RED, HIGH);

#ifdef ENABLE_PRINT
                    Update.printError(Serial);
#endif
                }
                else
                {
                    digitalWrite(LED_GREEN, HIGH);
                    doing_update = false;
                    doing_restart = true;
                }
            }
        }
        else
        {
            digitalWrite(LED_RED, HIGH);
            doing_update = false;
        }
    }
}

void onMqttPublish(uint16_t packetId)
{
    Serial.println("Publish acknowledged.");
    Serial.print("  packetId: ");
    Serial.println(packetId);
    last_publish_success_id = packetId;

    if (do_subscripe_firmwaretopic_id == packetId &&
        String(myappConfig.firmwaretopic) != "")
    {
        uint16_t packetIdSub1 = mqttClient.subscribe(
            String(myappConfig.firmwaretopic).c_str(), 2);
        Serial.print("Subscribing at QoS 2, packetId: ");
        Serial.println(packetIdSub1);
        do_subscripe_firmwaretopic_id = 0;
        last_mqtt_package_id = 0;
    }
}

bool setupWifiAndConfig(bool force)
{
    /*
    char get_ip[20] = "";
    char get_gw[20] = "";
    char get_sn[20] = "";
    char get_mqttHost[20] = "";
    char get_mqttPort[10] = "";
    char get_configTopic[50] = "";

    AsyncWiFiManagerParameter set_ip("IP", "ESP IP", get_ip, 20);
    AsyncWiFiManagerParameter set_gw("GW", "Gateway", get_gw, 20);
    AsyncWiFiManagerParameter set_sn("NM", "Netmasq", get_sn, 20);
    AsyncWiFiManagerParameter set_mqttHost("MQTT IP", "MQTT IP", get_mqttHost, 20);
    AsyncWiFiManagerParameter set_mqttPort("MQTT Port", "MQTT PORT 1883", get_mqttPort, 10);
    AsyncWiFiManagerParameter set_configTopic("Topic", "Topic with Configuration", get_configTopic, 50);
    */

    AsyncWiFiManager wifiManager(&server, &dns);

    //wifiManager.setConfigPortalTimeout(300);
    //set config save notify callback
    wifiManager.setSaveConfigCallback(saveConfigCallback);

    /*
    wifiManager.addParameter(&set_ip);
    wifiManager.addParameter(&set_gw);
    wifiManager.addParameter(&set_sn);
    wifiManager.addParameter(&set_mqttHost);
    wifiManager.addParameter(&set_mqttPort);
    wifiManager.addParameter(&set_configTopic);
    */
    bool success = false;
    if (force)
    {
        wifiManager.resetSettings();
        success = wifiManager.startConfigPortal("ForceConfigAP");
    }
    else
    {
        //wifiManager.setSTAStaticIPConfig(myhostConfig._ip, myhostConfig._gw, myhostConfig._sn);
        success = wifiManager.autoConnect(DEFAULT_HOSTNAME);
    }
    if (success && shouldSaveConfig)
    {
        /*
        myhostConfig._ip.fromString(String(set_ip.getValue()));
        myhostConfig._gw.fromString(String(set_gw.getValue()));
        myhostConfig._sn.fromString(String(set_sn.getValue()));
        myhostConfig.mqttHost.fromString(String(set_mqttHost.getValue()));
        myhostConfig.mqttPort = atoi(set_mqttPort.getValue());
        if(myhostConfig.mqttPort == 0){
            myhostConfig.mqttPort = 1883;
            Serial.println("mqtt port was 0");
        }
        strcpy(myhostConfig.configTopic, set_configTopic.getValue());

        myhostConfig.crc = crc((uint8_t *)&myhostConfig, sizeof(hostConfig));
        */
        /*
        EEPROM.put(0, myhostConfig);
        EEPROM.commit();
        */
        /*preferences.begin("hostconfig");
        preferences.putBytes("hostconfig", &myhostConfig, sizeof(hostConfig));
        preferences.end();
        Serial.println(myhostConfig.crc);
        */
    }

    return success;
}


void setup()
{
    WRITE_PERI_REG(RTC_CNTL_BROWN_OUT_REG, 0);
    pinMode(LED_GREEN, OUTPUT);
    digitalWrite(LED_GREEN, LOW);
     // Maybe for Battery
    //WRITE_PERI_REG(RTC_CNTL_BROWN_OUT_REG, 0); //disable brownout detector
    Serial.begin(115200);
    Serial.setDebugOutput(true);
    //timeout_millis = millis();

    //EEPROM.begin(sizeof(hostConfig) + sizeof(appConfig));
    
    Serial.print("Version: ");
    Serial.print(version);
    Serial.print(" ");
    Serial.println(buildtime);

    /*
    EEPROM.get(0, myhostConfig);
    EEPROM.get(sizeof(hostConfig), myappConfig);
    */
    preferences.begin("appconfig", true);
    preferences.getBytes("appconfig", &myappConfig, sizeof(appConfig));
    preferences.end();
    if (myappConfig.crc == crc((uint8_t *)&myappConfig, sizeof(appConfig)))
    {
        Serial.println(F("Valid APP Config readed"));
        Serial.println(myappConfig.crc);
        Serial.println(myappConfig.ir_on_difference);
        Serial.println(myappConfig.ir_on_pre_measure_min);
        Serial.println(myappConfig.ir_on_measure_max);
        Serial.println(myappConfig.ir_off_difference);
        Serial.println(myappConfig.ir_off_pre_measure_min);
        Serial.println(myappConfig.ir_off_measure_max);
        Serial.println(myappConfig.core_led_delay_ms);
        Serial.println(myappConfig.power_on_duration_ms);
        has_valid_appconfig = true;
    }
    else
    {
        Serial.println(F("No Valid APP Config, settings defaults"));
        myappConfig.firmwaretopic[0] = 0;
        myappConfig.ir_on_difference = DEFAULT_IR_ON_DIFFERENCE;
        myappConfig.ir_on_pre_measure_min = DEFAULT_IR_ON_PRE_MEASURE_MIN;
        myappConfig.ir_on_measure_max = DEFAULT_IR_ON_MEASURE_MAX;
        myappConfig.ir_off_difference = DEFAULT_IR_OFF_DIFFERENCE;
        myappConfig.ir_off_pre_measure_min = DEFAULT_IR_OFF_PRE_MEASURE_MIN;
        myappConfig.ir_off_measure_max = DEFAULT_IR_OFF_MEASURE_MAX;
        myappConfig.core_led_delay_ms = DEFAULT_CORE_LED_DELAY_MS;
        myappConfig.power_on_duration_ms = DEFAULT_POWER_ON_DURATION_MS;
    }

    setupApp();

    /*
    preferences.begin("hostconfig", true);
    preferences.getBytes("hostconfig", &myhostConfig, sizeof(hostConfig));
    preferences.end();
    if (myhostConfig.crc == crc((uint8_t *)&myhostConfig, sizeof(hostConfig)))
    {
        Serial.println(F("Valid Host Config readed"));
        Serial.println(myhostConfig.crc);

        mqttClient.onConnect(onMqttConnect);
        mqttClient.onDisconnect(onMqttDisconnect);
        mqttClient.onSubscribe(onMqttSubscribe);
        mqttClient.onUnsubscribe(onMqttUnsubscribe);
        mqttClient.onMessage(onMqttMessage);
        mqttClient.onPublish(onMqttPublish);
        WiFi.onEvent(WiFiEvent);

        mqttClient.setServer(myhostConfig.mqttHost, myhostConfig.mqttPort);
        if (digitalRead(BUTTON) == 0)
        {
            Serial.println("Force Wifi config Portal");
            if (!setupWifiAndConfig(true))
            {
                Serial.println("failed to connect and hit timeout in force on boot");
            }
        }
        else
        {
            //wifiManager.autoConnect(myhostConfig.hostName);
            if (!setupWifiAndConfig(false))
            {
                Serial.println("failed to connect and hit timeout");

            }
        }
    }
    else
    {
        Serial.println(F("Invalid Config readed"));
        Serial.println(myhostConfig.crc);

        if (!setupWifiAndConfig(true))
        {
            Serial.println("failed to connect and hit timeout");
            delay(3000);
        }
    }
    */
    mqttClient.onConnect(onMqttConnect);
    mqttClient.onDisconnect(onMqttDisconnect);
    mqttClient.onSubscribe(onMqttSubscribe);
    mqttClient.onUnsubscribe(onMqttUnsubscribe);
    mqttClient.onMessage(onMqttMessage);
    mqttClient.onPublish(onMqttPublish);
    WiFi.onEvent(WiFiEvent);

    if (digitalRead(BUTTON) == 0)
    {
        Serial.println("Force Wifi config Portal");
        if (!setupWifiAndConfig(true))
        {
            Serial.println("failed to connect and hit timeout in force on boot");
        }
    }
    else
    {
        //wifiManager.autoConnect(myhostConfig.hostName);
        if (!setupWifiAndConfig(false))
        {
            Serial.println("failed to connect and hit timeout");

        }
}
    if (shouldSaveConfig)
    {

    }


    Serial.print("IP address: ");
    Serial.println(WiFi.localIP());
    Serial.print("ESP Mac Address: ");
    Serial.println(WiFi.macAddress());
    Serial.print("Subnet Mask: ");
    Serial.println(WiFi.subnetMask());
    Serial.print("Gateway IP: ");
    Serial.println(WiFi.gatewayIP());

/*
    server.reset();

    // Simple Firmware Update Form
    server.on("/update", HTTP_GET, [](AsyncWebServerRequest *request) {
    request->send(200, "text/html", "<form method='POST' on='/update' enctype='multipart/form-data'><input type='file' name='update'><input type='submit' value='Update'></form>");
    });

    server.on("/update", HTTP_POST, [](AsyncWebServerRequest *request) {
    doing_restart = !Update.hasError();
    AsyncWebServerResponse *response = request->beginResponse(200, "text/plain", doing_restart?"OK":"FAIL");
    response->addHeader("Connection", "close");
    request->send(response); }, [](AsyncWebServerRequest *request, String filename, size_t index, uint8_t *data, size_t len, bool final) { updateFirmware(request, filename, index, data, len, final); });

    server.begin();
    */
   server.reset();
   AsyncElegantOTA.begin(&server); // Start ElegantOTA  
   server.begin();
}

void loop()
{
    if (force_config)
    {
        setupWifiAndConfig(true);
        digitalWrite(LED_GREEN, HIGH);
        doing_update = false;
    }
    /*
    if (myappConfig.loop_delay_ms >= 100)
    {
        Serial.print(millis());
        Serial.print(" in loop. ");
    }
    */
    if (mdns_startet){
        if(nrOfServices == 0){
            nrOfServices = MDNS.queryService("mqtt", "tcp");
   
            if (nrOfServices == 0) {
                Serial.println("No services were found.");
            }
            else
            {
                Serial.print("mqtt services: ");
                Serial.println(nrOfServices);
                Serial.print(MDNS.IP(0));
                Serial.print(":");
                Serial.println(MDNS.port(0));
                mqttClient.setServer(MDNS.IP(0), MDNS.port(0));
                mqtt_do_connection = true;
                mdns_startet = false;
            }           
        }
    } 

    if (mqtt_do_connection ||
        (last_mqtt_connect_try + 1000 * 60) < millis())
    {
        connectToMqtt();
    }
    if (mqttClient.connected() && send_event_message && !send_status_message)
    {
        //StaticJsonDocument<200> doc;
        doc_event.clear();
        doc_event["motor"] = true;
        doc_event["ir_pre_maesure"] = ir_pre_maesure;
        doc_event["ir_maesure"] = ir_maesure;
        doc_event["ir_diff"] = ir_diff;

        publishEvent(doc_event);
        send_event_message = false;
    }
    if (mqttClient.connected() && 
        send_status_message && 
        (last_status_message_millis + 10 * 1000) < millis())
    {
        if(publishStatus()){
            send_status_message = false;
            last_status_message_millis = millis();
        }
    }
    //delay(100);
    loopApp();
    //delay(myappConfig.loop_delay_ms);
    if (doing_restart){
        delay(5000);
        ESP.restart();
    }
}

