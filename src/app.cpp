#include <Arduino.h>
#include "config.h"

//TaskHandle_t appTask1;

uint16_t ir_pre_maesure = 0;
uint16_t ir_maesure = 0;
uint16_t ir_diff = 0;
//uint16_t power_on_counter = 0;
//uint16_t power_on_duration = 0;
bool pump_state = false;
bool pump_enable = true;
bool ir_sensor_free = true;
unsigned long ir_last_poweron_millis = 0;

void get_ir_values()
{
    //digitalWrite(LED_BLUE, LOW);

    digitalWrite(IR_RECEIVER_PIN, HIGH);
    delay(myappConfig.core_led_delay_ms);
    ir_pre_maesure = analogRead(IR_RECEIVER_ADC_PIN);

    digitalWrite(IR_TRANSMITTER_PIN, HIGH);
    delay(myappConfig.core_led_delay_ms);
    ir_maesure = analogRead(IR_RECEIVER_ADC_PIN);

    digitalWrite(IR_TRANSMITTER_PIN, LOW);
    digitalWrite(IR_RECEIVER_PIN, LOW);
    //digitalWrite(LED_BLUE, HIGH);
    //ir_last_measure_millis = millis();
}

bool calc_diff(){
    if (ir_maesure < ir_pre_maesure)
    {
        ir_diff = ir_pre_maesure - ir_maesure;
    }
    else
    {
        ir_diff = 0;
    }
}

bool get_button_state(){
    if (digitalRead(BUTTON) == LOW)
    {
        mqtt_do_connection = true;
        send_status_message = true;
        return true;
    }
    return false;
}

void switchPumpOff(){
    digitalWrite(PUMP, LOW);
    digitalWrite(LED_BLUE, HIGH);
    if (pump_state == true){
        Serial.println("PUMP OFF");
        if (mqttClient.connected())
        {
            StaticJsonDocument<150> doc;
            doc["motor"] = false;
            doc["ir_pre_maesure"] = ir_pre_maesure;
            doc["ir_maesure"] = ir_maesure;
            doc["ir_diff"] = ir_diff;
            doc["ir_last_poweron_millis"] = millis() - ir_last_poweron_millis;
            publishEvent(doc);
        }
        ir_last_poweron_millis = millis();
    }
    pump_state = false;
}

void switchPumpOn(){
    digitalWrite(PUMP, HIGH);
    digitalWrite(LED_BLUE, LOW);
    //Serial.println(" PUMP ON!");
    //ir_sensor_free = false;
    ir_last_poweron_millis = millis();
    //timeout_millis = millis();
    //power_on_counter++;
    //send_action_message = true;
    
    if (mqttClient.connected() && pump_state == false)
    {
        ///StaticJsonDocument<200> doc;
        doc_event.clear();
        doc_event["motor"] = true;
        doc_event["ir_pre_maesure"] = ir_pre_maesure;
        doc_event["ir_maesure"] = ir_maesure;
        doc_event["ir_diff"] = ir_diff;

        publishEvent(doc_event);
    }
    else
    {
    }
    pump_state = true;    
}

/*
void check_ir_and_switch_pump()
{
    get_ir_values();
    if (ir_maesure < ir_pre_maesure)
    {
        ir_diff = ir_pre_maesure - ir_maesure;
    }
    else
    {
        ir_diff = 0;
    }
    if (
        ir_diff > myappConfig.ir_action_difference &&
        ir_pre_maesure > myappConfig.ir_action_pre_measure_min &&
        ir_maesure < myappConfig.ir_action_measure_max)
    {
        switchPumpOn();
    }
    else
    {
        switchPumpOff();
    }
}
*/
//void loopApp(void * pvParameters)
void loopApp()
{
    if (get_button_state()){
        switchPumpOn();
        ir_sensor_free = true;
        digitalWrite(LED_RED, HIGH);
    }
    else
    {
        get_ir_values();
        calc_diff();
        if (pump_state == false && pump_enable){
            if((ir_last_poweron_millis + 1000) < millis())
            {
                if (ir_sensor_free == true){
                    if (
                        ir_diff > myappConfig.ir_on_difference &&
                        ir_pre_maesure > myappConfig.ir_on_pre_measure_min &&
                        ir_maesure < myappConfig.ir_on_measure_max
                        )
                    {
                        switchPumpOn();
                    }
                }
                else
                {   
                    if (
                        //ir_diff < myappConfig.ir_on_difference &&
                        //ir_pre_maesure > myappConfig.ir_on_pre_measure_min &&
                        ir_maesure > myappConfig.ir_on_measure_max
                        )
                    {
                        digitalWrite(LED_RED, HIGH);
                        ir_sensor_free = true;
                    }
                }
            }
        }
        else if(pump_state == true)
        {
            if  (
                    ir_diff < myappConfig.ir_off_difference ||
                    ir_pre_maesure < myappConfig.ir_off_pre_measure_min ||
                    ir_maesure > myappConfig.ir_off_measure_max
                )
            {
                switchPumpOff();
            }
            else if (ir_last_poweron_millis + myappConfig.power_on_duration_ms < millis())
            {
                ir_sensor_free = false;
                digitalWrite(LED_RED, LOW);
                switchPumpOff();
            }
        }
        else
        {
            switchPumpOff();
        }
    }
}

/*
        if (pump_state == false && 
            power_on_counter < myappConfig.max_power_on_interval &&
            ir_last_measure_millis + myappConfig.ir_interval_measure_ms < millis())
        {
            check_ir_and_switch_pump();
        }
        if (get_button_state()){
            switchPumpOn();
        }
        if (((timeout_millis + myappConfig.delay_after_max_power_on_interval_ms) < millis()) &&
            power_on_counter >= myappConfig.max_power_on_interval)
        {
            get_ir_values();
            if (ir_maesure < ir_pre_maesure)
            {
                ir_diff = ir_pre_maesure - ir_maesure;
            }
            else
            {
                ir_diff = 0;
            }
            if ((ir_diff < myappConfig.ir_action_difference)
            // ||
            //    (ir_pre_maesure > myappConfig.ir_action_pre_measure_min &&
            //     ir_maesure < myappConfig.ir_action_measure_max)
            )
            {
                power_on_counter = 0;
            }
            delay(myappConfig.loop_delay_ms);

        }

        if (((timeout_millis + power_on_duration) < millis()) && pump_state)
        {
            Serial.println("PUMP OFF");
            switchPumpOff();
            //digitalWrite(PUMP, LOW);
            //digitalWrite(LED_RED, HIGH);
            //pump_state = false;
            ir_last_measure_millis = millis();
            delay(myappConfig.power_on_duration_ms);
            power_on_duration = myappConfig.power_on_duration_ms;
        }
    */
//    }
//}

void setupApp()
{
    pinMode(PUMP, OUTPUT);
    digitalWrite(LED_BLUE, HIGH);
    digitalWrite(LED_RED, HIGH);
    digitalWrite(LED_GREEN, HIGH);
    pinMode(LED_BLUE, OUTPUT);
    pinMode(LED_RED, OUTPUT);
    pinMode(LED_GREEN, OUTPUT);
    pinMode(IR_RECEIVER_PIN, OUTPUT);
    pinMode(IR_TRANSMITTER_PIN, OUTPUT);
    //get_ir_values();
    //power_on_duration = myappConfig.first_power_on_duration_ms;
    //check_ir_and_switch_pump();
    /*
    xTaskCreatePinnedToCore(loopApp,
                            "loopApp",
                            10000,
                            NULL,
                            7,
                            &appTask1,
                            0);
    */
}