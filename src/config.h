#pragma once
#include <Arduino.h>
//#include "crc.h"

// to fix error: no type named 'type' in 'struct ArduinoJson::Internals::EnableIf<false, String>'
//#define ARDUINOJSON_ENABLE_ARDUINO_STRING 1
#include <ArduinoJson.h>

#include <WiFi.h>
#include <string>
#include "Update.h"
#include <AsyncMqttClient.h>

#define DEFAULT_HOSTNAME "foamdispenser"

#define CONFIG_CHAR_LENGTH 40

extern const char *version;
extern const char *buildtime;

#define MAXTOPICLENGTH 128
#define MAXPAYLOADLENGTH 30

#define PUMP 26
#define LED_BLUE 32
#define LED_RED 25
#define LED_GREEN 33
#define BUTTON GPIO_NUM_14

//gpio34 adc channel 6 (same as in ulp)
#define IR_RECEIVER_ADC_PIN 34

//gpio27 led power (same as in ulp)
#define IR_TRANSMITTER_PIN 13
//gpio27 ir receiver power (same as in ulp)
#define IR_RECEIVER_PIN 27

//app config defaults
#define DEFAULT_CORE_LED_DELAY_MS 20
//#define DEFAULT_FIRST_POWER_ON_DURATION_MS 500
#define DEFAULT_POWER_ON_DURATION_MS 3000
//#define DEFAULT_MAX_POWER_ON_INTERVAL 2
//#define DEFAULT_DELAY_AFTER_MAX_POWER_ON_INTERVAL_MS 2000
//#define DEFAULT_LOOP_DELAY_MS 10

#define DEFAULT_IR_ON_DIFFERENCE 1000
#define DEFAULT_IR_ON_PRE_MEASURE_MIN 3000
#define DEFAULT_IR_ON_MEASURE_MAX 2500
#define DEFAULT_IR_OFF_DIFFERENCE 500
#define DEFAULT_IR_OFF_PRE_MEASURE_MIN 3000
#define DEFAULT_IR_OFF_MEASURE_MAX 2500
//#define DEFAULT_IR_INTERVAL_MEASURE_MS 500

#define uS_TO_S_FACTOR 1000000 /* Conversion factor for micro seconds to seconds */
//#define DEFAULT_TIME_TO_SLEEP 900 /* Time ESP32 will go to sleep (in seconds) */

/*
struct hostConfig
{
    IPAddress _ip;
    IPAddress _gw;
    IPAddress _sn;
    IPAddress mqttHost;
    uint16_t mqttPort;
    //char mqttUser[40];
    //char mqttPass[40];
    char configTopic[50];

    unsigned long crc;
};
*/

struct appConfig
{
    char firmwaretopic[MAXTOPICLENGTH] = "";
    uint16_t ir_on_difference = DEFAULT_IR_ON_DIFFERENCE;
    uint16_t ir_on_pre_measure_min = DEFAULT_IR_ON_PRE_MEASURE_MIN;
    uint16_t ir_on_measure_max = DEFAULT_IR_ON_MEASURE_MAX;
    uint16_t ir_off_difference = DEFAULT_IR_OFF_DIFFERENCE;
    uint16_t ir_off_pre_measure_min = DEFAULT_IR_OFF_PRE_MEASURE_MIN;
    uint16_t ir_off_measure_max = DEFAULT_IR_OFF_MEASURE_MAX;
    uint16_t core_led_delay_ms = DEFAULT_CORE_LED_DELAY_MS;
    uint16_t power_on_duration_ms = DEFAULT_POWER_ON_DURATION_MS;
    unsigned long crc = 0;
};

extern appConfig myappConfig;

extern RTC_DATA_ATTR int bootCount;
extern RTC_DATA_ATTR int32_t esp_timer_time;

extern uint16_t ulp_ir_pre_maesure;
extern uint16_t ulp_ir_maesure;
extern uint16_t ulp_ir_diff;

//extern unsigned long timeout_millis;
extern bool send_action_message;

extern AsyncMqttClient mqttClient;
extern bool mqtt_do_connection;
extern bool send_status_message;
extern StaticJsonDocument<200> doc_event;
extern uint16_t publishEvent(StaticJsonDocument<200> doc);

extern uint16_t ir_pre_maesure;
extern uint16_t ir_maesure;
extern uint16_t ir_diff;

extern void setupApp();
extern void loopApp();
extern void switchPumpOff();
