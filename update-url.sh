#!/bin/bash

#hostname=$1
hostname=192.168.7.130

md5=$(md5sum .pio/build/esp32dev/firmware.bin | awk '{print $1}')

curl  --progress-bar \
      -F 'name=update' \
      -F "MD5=$md5" \
      -F 'filename=@.pio/build/esp32dev/firmware.bin'  \
      "http://$hostname/update"


